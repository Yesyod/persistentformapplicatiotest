package com.celltick.candidates.yeshayahudeutsch.presistentformapp;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText mUserEditText;
    private EditText mPasswordEditText;
    private Button mSaveButton;
    private Button mLoadButton;
    private boolean isPortraitMode = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initOrientation();
        initView();
    }

    private void initOrientation() {
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isPortraitMode = false;
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT){
            isPortraitMode = true;
        }
    }

    private void initView() {
        mUserEditText = (EditText)findViewById(R.id.user);
        mPasswordEditText = (EditText)findViewById(R.id.password);

        mSaveButton = (Button)findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDetails();
            }
        });

        mLoadButton = (Button)findViewById(R.id.load_button);
        mLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = mUserEditText.getText().toString();
                String password = mPasswordEditText.getText().toString();
                if (!TextUtils.isEmpty(user) || !TextUtils.isEmpty(password)) {
                    showDialog();
                }else{
                    loadDetails();
                }
            }
        });
    }


    private void saveDetails() {
        final String user = mUserEditText.getText().toString();
        final String password = mPasswordEditText.getText().toString();
        if (TextUtils.isEmpty(user)) {
            showMessage("Not a valid user name!");
        }else if(TextUtils.isEmpty(password)){
            showMessage("Not a valid password!");
        }else{
            if(isPortraitMode){
                saveDetailsToJSON(user, password);
            }else{
                saveDetailsToSharedPre(user, password);
            }
        }
    }


    private void saveDetailsToJSON(final String user, final String password) {


        new AsyncTask<Void, Void, String>() {
            JSONObject countryObj = new JSONObject();
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    countryObj.put("user", user);
                    countryObj.put("pass", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                JSONmanager.saveData(getApplicationContext(),countryObj.toString());
                return null;
            }

            @Override
            protected void onPostExecute(String data) {
                super.onPostExecute(data);
                showMessage("save JSON");
                mUserEditText.setText("");
                mPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());
                mPasswordEditText.setText("");
            }
        }.execute();


    }

    private void saveDetailsToSharedPre(String user, String password) {
        new SharedPreManager(this).saveDetails(user, password);
        showMessage("saveSharedPre");
        mUserEditText.setText("");
        mPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());
        mPasswordEditText.setText("");
    }

    private void loadDetails() {
        if(isPortraitMode){
            loadDetailsFromJSON();
        }else{
            loadDetailsFromSharedPre();
        }

    }

    private void loadDetailsFromJSON() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                String mData = JSONmanager.getData(getApplicationContext());
                return mData;
            }

            @Override
            protected void onPostExecute(String data) {
                super.onPostExecute(data);
                if(data == null){
                    showMessage("no data");
                    mUserEditText.setText("");
                    mPasswordEditText.setText("");
                }else{
                    JSONObject countryObj = null ;
                    try {
                        countryObj = new JSONObject(data);
                        mUserEditText.setText(countryObj.get("user").toString());
                        mPasswordEditText.setTransformationMethod(null);
                        mPasswordEditText.setText(countryObj.get("pass").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    showMessage("loadFromJSON");
                }
            }
        }.execute();

    }

    private void loadDetailsFromSharedPre() {
        SharedPreManager sharedPreManager = new SharedPreManager(this);
        mUserEditText.setText(sharedPreManager.getUser());
        mPasswordEditText.setTransformationMethod(null);
        mPasswordEditText.setText(sharedPreManager.getPassword());
        showMessage("loadFromSharedPre");
    }

    private void showMessage(String message) {
        if (message != null) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }
    }
    private void showDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Overwrite data");
        alertDialogBuilder.setMessage(" Are you sure ?");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                loadDetails();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"You clicked over No",Toast.LENGTH_SHORT).show();
            }
        });
        Glide.with(this)
                .load("https://i.imgur.com/yaBgf27.png")
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        alertDialogBuilder.setIcon(resource);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        Log.i("GLIDE", "onResourceReady:");
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });

    }

}
