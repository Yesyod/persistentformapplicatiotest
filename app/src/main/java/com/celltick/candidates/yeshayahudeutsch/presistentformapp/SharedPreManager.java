package com.celltick.candidates.yeshayahudeutsch.presistentformapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shayd on 14/06/2017.
 */

public class SharedPreManager {
    Context context;

    SharedPreManager(Context context) {
        this.context = context;
    }

    public void saveDetails(String user, String password ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("User", user);
        editor.putString("Password", password);
        editor.commit();
    }

    public String getUser() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("User", "");
    }
    public String getPassword(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Password", "");
    }

}
